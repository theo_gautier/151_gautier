<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 08.03.2019
 * Time: 11:21
 * File: snows.php
 */

// tampon de flux stocké en mémoire
ob_start();
$titre="RentASnow - Nos snowboards";
?>
    <div class="span12" id="divMain">

        <article>
            <header>
                <h2> Nos snows</h2>

                    <div class="yox-view">

                        <div class="row-fluid">
                            <ul class="thumbnails">
                                <?php
                                echo $htmlresult;
                                ?>
                            </ul>
                        </div>
                    </div>
            </header>
        </article>
<?php
$contenu = ob_get_clean();
require "gabarit.php";