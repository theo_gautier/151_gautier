<?php

// tampon de flux stocké en mémoire
ob_start();
$titre="RentASnow - Accueil";
if(isset($_POST["registererror"])){
    echo "<div style='color: red; font-style: inherit'>inscription refusée</div>";
    $_POST["registererror"]= NULL ;
}
?>

    <form action="index.php?action=register" method="post" name="formRegister" >

        <div class="form-group">
            <label for="inputEmail" class="form-check-label">Email address *</label>
            <input type="email" id="inputEmail" aria-describedby="emailHelp" placeholder="firstname@domain.ch" required class="form-control" id="InputEmail" name="InputEmail">
            <small id="emailHelp" class="form-text text-muted" >We'll never share your email with anyone else.</small>
        </div>
        <div class="form-group">
            <label for="inputPseudo" class="form-check-label">Username *</label>
            <input type="text" id="inputPseudo" placeholder="Pseudo" required class="form-control" name="inputPseudo">
        </div>
        <div class="form-group">
            <label for="inputPassword" class="form-check-label">Password *</label>
            <input type="password" id="inputPassword" placeholder="Password" required class="form-control" id="InputPassword" name="InputPassword">
        </div>
        <div class="form-group">
            <label for="inputPassword" class="form-check-label">Reapeat Password *</label>
            <input type="password" id="inputPasswordRepeat" placeholder="Reapeat Password" required class="form-control" name="InputPasswordRepeate">
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

<?php
$contenu = ob_get_clean();
require "gabarit.php";