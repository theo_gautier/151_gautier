<?php
/**
 * Created by PhpStorm.
 * User: Theo.GAUTIER
 * Date: 08.03.2019
 * Time: 11:21
 * File: snows.php
 */

// tampon de flux stocké en mémoire
ob_start();
$titre="RentASnow - Nos snowboards";
?>
    <article>
        <header>
            <h2> Nos snows</h2>
            <div class="table-responsive">
                <table class="table textcolor">
                    <tr>
                        <th>Code</th><th>Marque</th><th>Modèle</th><th>Longueur</th><th>Prix</th><th>Disponibilité</th><th>Photo</th>
                    </tr>
                                <?php
                                echo $htmlresult;
                                ?>
                </table>
            </div>
        </header>
    </article>
<?php
$contenu = ob_get_clean();
require "gabarit.php";