<?php
/**
 * Created by PhpStorm.
 * User: theo.gautier
 * Date: 15.02.2019
 * Time: 08:14
 * File: index.php
 */
session_start();
require "controler/controler.php"; /* appel du controller */

extract($_GET); /* axtraire les variables en GET */


/* si index.php est appelé et que soit $action n'est pas défini ou que $action == "home" afficher home.php (via controller.php) */
if(isset($_GET['action'])){
    $actionget = $_GET['action'];

    switch ($actionget){
        case 'home':
            $_GET['action'] = 'home';
            showHome();
            break;
        case 'login':
            $_GET['action'] = 'login';
            showLogin();
            break;
        case 'logout':
            $_GET['action'] = 'logout';
            logout();
            break;
        case 'snows':
            $_GET['action'] = 'snows';
            snowTable();
            break;
        case 'register':
            $_GET['action'] = 'register';
            register();
            break;
        case 'snowLeasingRequest':
            $_GET['action'] = 'snowLeasingRequest';
            snowLeasingRequest($_GET);
            break;
        case 'updateCartRequest':
            updateCartRequest($_GET);
            break;
        default:
            $_GET['action'] = 'home';
            showHome();
            break;
    }
}else{
    $_GET['action'] = 'home';
    showHome();
}


