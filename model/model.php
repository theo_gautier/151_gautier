
<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : introPDO
 * Created  : 28.01.2019 - 20:13
 *
 *
 * Last update :    [15.02.19 theo.gautier@cpnv.ch]
 *                  [adapt the code for the project snow]
 * Git source  :    [link]
 */

//Test client - must be remove to before using in a project
//try to get a list of available tables in database

/*
 * fonction qui va demander les informations sur un utilisateur à la base de donnée
 */

/**
 * @param $user "adresse email de l'utilisateur"
 * @return string "retourne soit les infos de l'utilisateur soit une erreur"
 */
function requestBdLogin($user)
{

    $query = 'select pseudo, userEmailAddress,userPsw,role  from users where userEmailAddress="' . $user .'";';
    $queryResult = executeQuerySelect($query);
    if(isset($queryResult[0])){
        $result = $queryResult[0];
    }else{
        $result = "errornone";
    }

    return $result;
}

/*
 * fonction qui va chercher toutes les information sur les snowboards à la base de donnée
 */
function requestSnowboards(){
    $query = 'select * from snows';
    $querysresult = executeQuerySelect($query);

    return $querysresult;
}

/*
 * fonction qui inscrit les nouvelles données d'un utilisateur dans la base de donnée
 */
/**
 * @param $userEmail "adresse email de l'utilisateur"
 * @param $userPseudo "pseudo de l'utilisateur"
 * @param $userPaswdHash "mot de passe en hash de l'utilisateur"
 * @return bool|null "retourne true si succès et false si échec"
 */
function requestBdRegister($userEmail,$userPseudo,$userPaswdHash){
    $query = 'insert into users (userEmailAddress,pseudo,userPsw,role) values ("'. $userEmail.'","'.$userPseudo.'","'.$userPaswdHash.'",0);';
    $result = executeQueryInsert($query);
    return $result;
}

requestSnowboard("B126");
function requestSnowboard($code){
    $query = 'select * from snows where code="'.$code.'";';
    $querysresult = executeQuerySelect($query);

    if(isset($querysresult[0]['ID'])) {
        $snowInfo['ID'] = $querysresult[0]['ID'];
        $snowInfo['code'] = $querysresult[0]['code'];
        $snowInfo['marque'] = $querysresult[0]['marque'];
        $snowInfo['model'] = $querysresult[0]['model'];
        $snowInfo['longueur'] = $querysresult[0]['longueur'];
        $snowInfo['dispo'] = $querysresult[0]['dispo'];
        $snowInfo['description'] = $querysresult[0]['description'];
        $snowInfo['prix'] = $querysresult[0]['prix'];
        $snowInfo['photo'] = $querysresult[0]['photo'];
        $snowInfo['active'] = $querysresult[0]['active'];
        $snowInfo['erreur'] = '0';
    }else{
        $snowInfo['erreur'] = '1';
    }
    return $snowInfo;
}




/*
 * fonction qui envoie toutes les requetes Select SQL à la base de données
 */
//Source : http://php.net/manual/en/pdo.prepare.php
/**
 * @param $query "requête SQL"
 * @return array|null "tableau avec le résultat de la requete"
 */
function executeQuerySelect($query)
{
    $queryResult = null;

    $dbConnexion = openDBConnexion();//open database connexion
    if ($dbConnexion != null)
    {
        $statment = $dbConnexion->prepare($query);//prepare query
        $statment->execute();//execute query
        $queryResult = $statment->fetchAll();//prepare result for client
    }
    $dbConnexion = null;//close database connexion
    return $queryResult;
}

/*
 * fonction qui envoie toutes les requetes insert SQL à la base de données
 */
/**
 * @param $query "requête SQL"
 * @return bool|null "retourne true si succès et false si échec"
 */
function executeQueryInsert($query)
{
    $queryResult = null;

    $dbConnexion = openDBConnexion();//open database connexion
    if ($dbConnexion != null)
    {
        $statment = $dbConnexion->prepare($query);//prepare query
        $queryResult =$statment->execute();//execute query

    }
    $dbConnexion = null;//close database connexion
    return $queryResult;
}

//Source : http://php.net/manual/en/pdo.construct.php

/*
 * fonction qui ouvre la connection à la base de donnée
 */
/**
 * @return null|PDO "retourne la connection à la base de donnée"
 */
function openDBConnexion ()
{
    $tempDbConnexion = null;

    $sqlDriver = 'mysql';
    $hostname = 'localhost';
    $port = 3306;
    $charset = 'utf8';
    $dbName = 'snows_tgr';
    $userName = 'snow';
    $userPwd = 'snow';
    $dsn = $sqlDriver . ':host=' . $hostname . ';dbname=' . $dbName . ';port=' . $port . ';charset=' . $charset;

    try{
        $tempDbConnexion = new PDO($dsn, $userName, $userPwd);
    }
    catch (PDOException $exception) {
        echo 'Connection failed: ' . $exception->getMessage();
    }
    return $tempDbConnexion;
}