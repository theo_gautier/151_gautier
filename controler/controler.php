<?php
/**
 * Author   : nicolas.glassey@cpnv.ch
 * Project  : 151_2019_ForStudents
 * Created  : 05.02.2019 - 18:40
 *
 * Last update :    [01.12.2018 author]
 *                  [add $logName in function setFullPath]
 * Git source  :    [link]
 */
/*
 * fonction qui sert à afficher la page "home"
 */

function showHome(){
    $_GET['action'] = 'home';
    require "view/home.php";
}

//region session

/*
 *  fonction qui soit affiche le formulaire de login soit enclanche le mecanisme de login
 */
/**
 *
 */
function showLogin(){
    if(!isset($_POST['InputEmail'])){ //
        require "view/login.php";
    }else{
        require "model/model.php";
        $userEmail = $_POST['InputEmail'];
        $userpswd = $_POST['InputPassword'];

        loginsession($userEmail,$userpswd); //appel la fonction loginsession dans le modèle (model/model.php)



    }

}

/*
 * fonction pour le mecanisme de login et le création des sessions
 *
 * paramètres: email de l'utilisateur ($userEmail), mot de passe de l'utilisateur ($userpswd)
 */
/**
 * @param $userEmail "Email de l'utilisateur"
 * @param $userpswd "mot de passe de l'utilisateur"
 */
function loginsession($userEmail,$userpswd){
    $requestresult = requestBdLogin($userEmail); // appel la fonction requestBdLogin et récupère le retour. Fonction dans le modèle (model/model.php)
    if($requestresult != "errornone") {
        if (password_verify($userpswd, $requestresult['userPsw'])) {
            $_SESSION['UserEmail'] = $requestresult['userEmailAddress'];
            $_SESSION['UserPseudo'] = $requestresult['pseudo'];
            $_SESSION['UserRole'] = $requestresult['role'];
            showHome(); // appel la fonction showHome qui affiche la page "home". Fonction dans ce fichier.
        } else {
            $_POST["loginerror"]= "wrong email or password";
            require "view/login.php";
        }
    }else{
        $_POST["loginerror"]= "wrong email or password";
        require "view/login.php";
    }
}

/*
 * fonction pour le mécanisme de logout
 */

/**
 *
 */
function logout(){
    $_SESSION= array();
    session_destroy();

    showHome(); // appel la fonction showHome qui affiche la page "home". Fonction dans ce fichier.
}
/*
 * fonction qui soit affiche le formulaire de register soit met en forme les données pour envoyer au modèle pour enregistrer l'utilisateur dans la base de donnée
 */
/**
 *
 */
function register(){
    if(!isset($_POST['InputEmail'])){
        require "view/register.php";
    }else{
        $userEmail = $_POST['InputEmail'];
        $userPseudo = $_POST['inputPseudo'];
        $userpswd = $_POST['InputPassword'];
        $userpswdRespeat = $_POST['InputPasswordRepeate'];

        if($userpswd != $userpswdRespeat){
            $_POST['registererror']="1";
            require "view/register.php";
        }else{
            require "model/model.php";
            $hashpassword = password_hash($userpswd,PASSWORD_DEFAULT);
            $result = requestBdRegister($userEmail,$userPseudo,$hashpassword);

            if($result == true){
                loginsession($userEmail,$userpswd); //appel la fonction loginsession dans le modèle (model/model.php)
            }else{
                $_POST['registererror']="1";
                require "view/register.php";
            }



        }
    }
}
//endregion


//region snowtable
/*
 * fonction pour l'affichage des snowboards
 *
 * gère l'affichage en fonction des roles
 */
function snowTable(){
    require "model/model.php";

    $resultatquery = requestSnowboards();
    if(isset($_SESSION['UserRole'])) {
        if ($_SESSION['UserRole'] == 1) {
            snowAdminTable($resultatquery); // fonction qui affiche la table pour le commercant. fonction dans ce fichier
        }else{
            snowPublicTable($resultatquery); // fonction qui affiche la table pour le commercant. fonction dans ce fichier
        }
    }else{
        snowPublicTable($resultatquery); // fonction qui affiche la table pour les utilisateurs. Fonction dans ce fichier
    }
}

// fonction pour le corps de la page si le role correspond à celui du vendeur
/**
 * @param $resultatquery "informations sur les snowboards"
 */
function snowAdminTable($resultatquery){
    $htmlresult = "";
    foreach ($resultatquery as $resultline) {
        $htmlresult .= "<tr><td>" . $resultline['code'] . "</td><td>" . $resultline['marque'] . "</td><td>" . $resultline['model'] . "</td><td>" . $resultline['longueur'] . " cm</td><td>CHF " . $resultline['prix'] . ".- par jour</td><td>" . $resultline['dispo'] . "</td><td><img src='".$resultline['photo']."_small.jpg' style='height: 20px'> </td></tr>";
    }
    require "view/snowsadmin.php";
}

// fonction pour le corps de la page si le role correspond à celui d'un utilisateur normal qu'il soit connecté ou non
/**
 * @param $resultatquery "informations sur les snowboards"
 */
function snowPublicTable($resultatquery){
    $htmlresult = "";
    foreach ($resultatquery as $resultline) {
        $htmlresult .= "<ul class='thumbnails'><li class='span3'><div class='thumbnail'><a href='".$resultline['photo'].".jpg' target='blank'><img src='".$resultline['photo']."_small.jpg'> </a><div class='caption'><h3>".$resultline['code']."</h3><p><strong>Marque : </strong>".$resultline['marque']."</p><p><strong>Modèle : </strong>".$resultline['model']."</p><p><strong>Longeur : </strong>".$resultline['longueur']." cm</p><p><strong>Prix : </strong> CHF ".$resultline['prix']." / jour</p><p><strong>Disponibilité : </strong>".$resultline['dispo']."</p><p><a href=\"index.php?action=snowLeasingRequest&code=".$resultline['code']."\" class=\"btn btn-primary\">Louer ce snow</a></p></div>  </div></li>";
    }
    require "view/snowspublic.php";
}

function snowLeasingRequest($varGet){

    $code = $varGet['code'];

    require_once "model/model.php";
     $result = requestSnowboard($code);
     require "view/snowLeasingRequest.php";
}

function updateCartRequest($varGet){
    $code = $varGet['code'];

    $quentity = $_POST['inputQuantity'];
    $Days = $_POST['inputDays'];

    $leasing['code'] = $varGet['code'];
    $leasing['qty'] = $_POST['inputQuantity'];
    $leasing['nbD'] = $_POST['inputDays'];
    $leasing['dateD'] = date("d-m-y");


    if(isset($_SESSION['cart'])){
        $_SESSION['cart'][] = $leasing;
    }else{
        $_SESSION['cart'] = array();
        $_SESSION['cart'][] = $leasing;
    }
    require "view/snowPanier.php";
}
//endregion