-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           8.0.12 - MySQL Community Server - GPL
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Export de la structure de la base pour snows_tgr
DROP DATABASE IF EXISTS `snows_tgr`;
CREATE DATABASE IF NOT EXISTS `snows_tgr` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `snows_tgr`;

-- Export de la structure de la table snows_tgr. snowboards
DROP TABLE IF EXISTS `snowboards`;
CREATE TABLE IF NOT EXISTS `snowboards` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `marque` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `longueur` float NOT NULL,
  `prix` float DEFAULT '0',
  `dispo` int(11) NOT NULL,
  `photo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Export de données de la table snows_tgr.snowboards : ~2 rows (environ)
DELETE FROM `snowboards`;
/*!40000 ALTER TABLE `snowboards` DISABLE KEYS */;
INSERT INTO `snowboards` (`ID`, `code`, `marque`, `model`, `longueur`, `prix`, `dispo`, `photo`) VALUES
	(1, 'B101', 'Burtom', 'Custom', 160, 29, 22, 'view/content/images/B101'),
	(2, 'B126', 'Burton', 'Free Thinker', 165, 45, 2, 'view/content/images/B126');
/*!40000 ALTER TABLE `snowboards` ENABLE KEYS */;

-- Export de la structure de la table snows_tgr. snows
DROP TABLE IF EXISTS `snows`;
CREATE TABLE IF NOT EXISTS `snows` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(4) NOT NULL,
  `marque` varchar(20) NOT NULL,
  `model` varchar(30) NOT NULL,
  `longueur` int(4) unsigned NOT NULL,
  `dispo` smallint(6) NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL DEFAULT '0',
  `prix` float unsigned NOT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `active` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `snow_code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- Export de données de la table snows_tgr.snows : ~9 rows (environ)
DELETE FROM `snows`;
/*!40000 ALTER TABLE `snows` DISABLE KEYS */;
INSERT INTO `snows` (`ID`, `code`, `marque`, `model`, `longueur`, `dispo`, `description`, `prix`, `photo`, `active`) VALUES
	(1, 'B101', 'Burton', 'Custom', 160, 22, 'La board la plus fiable de tous les temps, la solution snowboard pour tous les terrains. (Homme)', 29, 'view/content/images/B101', 1),
	(2, 'B126', 'Burton', 'Free Thinker', 165, 2, 'Élargissez votre vision grâce son interprétation du ride tout terrain dynamique sur la poudreuse. (Homme)', 45, 'view/content/images/B126', 1),
	(3, 'B327', 'Burton', 'Day Trader', 155, 6, 'Flottabilité sans effort et un contrôle qui renforce la confiance en soi. (Femme)', 25, 'view/content/images/B327', 0),
	(4, 'K266', 'K2', 'Wildheart', 152, 2, 'Keeping in versatile style (Femme)', 29, 'view/content/images/K266', 1),
	(5, 'N100', 'Nidecker', 'Tracer', 164, 11, 'Une expérience de carve hors du commun. Idéal pour carver comme jamais (Homme et femme)', 39, 'view/content/images/N100', 1),
	(6, 'N754', 'Nidecker', 'Ultralight', 166, 26, 'A la pointe de la technologie. Idéal pour le freeride sur les faces engagées (Homme et femme)', 59, 'view/content/images/N754', 1),
	(7, 'P067', 'Prior', 'Brandwine 153', 154, 9, 'High performance, directional Freeride board, draws a smooth, stable and fast line through all snow conditions. (Femme)', 49, 'view/content/images/P067', 1),
	(8, 'P165', 'Prior', 'BC Split 161', 169, 1, 'Sa forme directionnelle Freeride offre une ride plutôt douce et stable dans une variété de conditions', 35, 'view/content/images/P165', 1),
	(9, 'K409', 'K2', 'Lime Lite', 149, 15, 'Best For Freestyle Evolution with a Focus on Fun (Femme)', 55, 'view/content/images/K409', 1);
/*!40000 ALTER TABLE `snows` ENABLE KEYS */;

-- Export de la structure de la table snows_tgr. users
DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userEmailAddress` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `userPsw` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pseudo` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `userEmailAddress` (`userEmailAddress`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Export de données de la table snows_tgr.users : ~3 rows (environ)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `userEmailAddress`, `userPsw`, `pseudo`, `role`) VALUES
	(1, 'testUser@cpnv.ch', '$2y$10$xRprWebBg8h58VJqgDzHAuJizm8YMbrSvXkq6eovCxi9yqBp1lk4i', 'testPseudo', 0),
	(4, 'legrand@grand.ch', '$2y$10$xi5wDnDkbf/MI.os5DUQM.nl6JYx.7dbaZOET18H3UVix3LeiOnp2', 'legrand', 0),
	(27, 'admin@admin.local', '$2y$10$s8RVY7obf8tHyzYjIU4H/.9ZXLefpCk4oc1oEIelswcL1uaceUZ0.', 'admin', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
